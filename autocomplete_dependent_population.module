<?php
/**
 * @file
 * Enables the autocomplete dependent field population functionality.
 * @author Debarya Das
 */

/**
 * Implements hook_element_info().
 */
function autocomplete_dependent_population_element_info() {
  $element['autocomplete_dependee'] = array(
    '#input' => TRUE,
    '#process' => array('autocomplete_dependent_population_generate_element'),
    '#path' => NULL,
    '#theme' => 'autocomplete_dependee_field',
    '#theme_wrappers' => array('form_element'),
  );

  return $element;
}

/**
 * This function returns a new form element for  autocomplete functionality.
 *
 * @param array $element
 *   An array containing the autocomplete_dependee element.
 * @param array $form_state
 *   An array containing the form submit values.
 * @param array $complete_form
 *   An array containing the form.
 *
 * @return array
 *   A form element of  autocomplete_dependee type.
 */
function autocomplete_dependent_population_generate_element(array $element, array $form_state = array(), array $complete_form = array()) {
  $path = drupal_get_path('module', 'autocomplete_dependent_population');
  if (isset($element['#type'])) {
    $counter = drupal_static(__FUNCTION__, 0);
    $settings[$counter] = array(
      'url' => isset($element['#path']) ? $element['#path'] : '',
      'keyupField' => isset($element['#name']) ? $element['#name'] : '',
    );

    if (isset($element['#name'])) {
      $element[$element['#name']] = array(
        '#type' => 'textfield',
        '#title' => isset($element['#title']) ? $element['#title'] : '',
        '#path' => isset($element['#path']) ? $element['#path'] : '',
        '#title_display' => 'invisible',
        '#attached' => array(
          'library' => array(
            array('system' , 'ui.autocomplete'),
          ),
          'js' => array(
            0 => array(
              'data' => array('autocomplete_dependent_population' => $settings),
              'type' => 'setting',
            ),
            $path . '/js/autocomplete_dependent_population.js' => array(
              'type' => 'file',
            ),
          ),
        ),
      );

      $counter++;
    }
  }

  return $element;
}

/**
 * Implements hook_theme().
 */
function autocomplete_dependent_population_theme() {
  return array(
    'autocomplete_dependee_field' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * This function provides the theming of autocomplete dependee variable.
 *
 * @param array $variables
 *   An array containing the autocomplete dependee variable.
 *
 * @return output
 *   The themed otput of the autocomplete dependee variable.
 */
function theme_autocomplete_dependee_field(array $variables) {
  return drupal_render($variables['element'][$variables['element']['#name']]);
}
